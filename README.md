# Colab Neural Style

A Google Colab Notebook implementing the Neural Style transfer written by [Anish Athalye](https://github.com/anishathalye).


# How to:
0.  go to your Google Drive in a browser of your choice and start and upload the .ipynb file of this repo
1.  open the styleTransfer.ipynb file in the web browser
2.  activate GPU in Settings
    1. click Runtime
    2. change runtime type
    3. set Hardware accelerator to GPU
3.  mount Drive in left Sidebar
    1. click Files
    2. Mount Drive
4.  specify link to content and style image. Change "XY" of the followinng lines: `!wget -O input/style.jpg "XY"` AND `!wget -O input/content.jpg "XY"`
5.  Run the Style Transfer
    1. click Runtime
    2. click Run all
6.  Wait till it is finsished. The Output image will be copied to your Google Drive home directory

# Credits
- Thanks to [Anish Athalye](https://github.com/anishathalye) for [Neural Style](https://github.com/anishathalye/neural-style). Copyright (c) 2015 Anish Athalye. Released under GPLv3
- Thanks to [A. Vedaldi and B. Fulkerson](http://www.vlfeat.org/) for the [pre-trained VGG network](https://www.vlfeat.org/matconvnet/pretrained/)